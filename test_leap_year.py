import unittest

from leap_year import is_leap_year


class TestLeapYear(unittest.TestCase):
    def test_1600(self):
        self.assertTrue(is_leap_year(1600))

    def test_1981(self):
        self.assertFalse(is_leap_year(1981))

    def test_2008(self):
        self.assertTrue(is_leap_year(2008))

    def test_1900(self):
        self.assertFalse(is_leap_year(1900))