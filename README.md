Leap Year
=========

Write a function that returns true if the year passed is a leap year

Definition
----------

A leap year must be:

- Divisible by 4;
- Not divisible by 100 but divisible by 400.

Some leap years:
- 1600
- 1732
- 1888
- 2008

Non leap years:
- 1900
- 1951
- 1889
- 2001